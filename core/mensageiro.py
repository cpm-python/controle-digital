import threading
import binascii
import datetime
import json
import os
import unittest

import requests
import time

url_generica = 'https://discord.com/api/webhooks/845444207866282016/' \
                 'UpZsgx5s6vCB3Lr9l5j7MFK5PgQ4jkYadixBu7nX3H5KI3qhgVF6VyS-EE9PDaqP961s'


def gerar_cor_aleatoria():
    return int(binascii.b2a_hex(os.urandom(3)), 16)


class Discord:
    def __init__(self, url_mensageiro=None):
        self._url_mensageiro = url_mensageiro if url_mensageiro is not None else url_generica

    def post(self, body):
        dumps = json.dumps(body)
        r = requests.post(url=self._url_mensageiro,
                          headers={'content-type': 'application/json'},
                          data=dumps)

        if r.status_code != 204:
            pass
            # print(dumps)
            # print(f'{r} - {json.dumps(r.json())}')

        time.sleep(1)

    def enviar(self, texto_para_enviar, cor=None, titulo=None):

        model = {
            "title": titulo if titulo is not None else "Controle Digital",
            "type": "rich",
            "description": texto_para_enviar[:2048],
            "url": None,
            "timestamp": datetime.datetime.utcnow().replace(tzinfo=datetime.timezone.utc).isoformat(),
            "color": cor if cor is not None else gerar_cor_aleatoria(),
            "footer": {
                "text": None
            },
            "image": None,
            "video": None,
            "author": {
                "name": None,
                "url": None,
                "icon_url": None
            }
        }

        threading.Thread(target=self.post, args=({"embeds": [json.loads(json.dumps(model))]},)).start()

    def enviar_simples(self, mensgem_simples: str):

        threading.Thread(target=self.post, args=({"content": mensgem_simples},)).start()
