from django.test import TestCase
from core.log import LOG


class LogTestCase(TestCase):

    def setUp(self):
        print(self)
        self.log = LOG("LogTestCase")

    def test_info(self):
        self.log.info("info")

    def test_warning(self):
        self.log.warning("warning")

    def test_error(self):
        self.log.error("error")

    def test_critical(self):
        self.log.critical("critical")
