from django.test import TestCase


def add_num(num):
    return num + 1


class SimplesTestCase(TestCase):
    # roda toda vez
    def setUp(self):
        self.valor = 1
        self.valor_final = 2

    # testa a unidade do codigo
    def test_add_num_true(self):
        valor = add_num(self.valor)
        self.assertTrue(valor == self.valor_final)

    def test_add_num_equal(self):
        valor = add_num(self.valor)
        self.assertEqual(valor, self.valor_final, f'o retorno deve ser {self.valor_final}')
