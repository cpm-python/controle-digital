from model_mommy import mommy
from django.test import TestCase

from core.models import *


class OrgaoTestCase(TestCase):
    def setUp(self) -> None:
        self.orgao = mommy.make(Orgao)

    def test_str(self):
        self.assertEqual(self.orgao.codigo, str(self.orgao))

    def test_update(self):
        orgao = Orgao()
        orgao.codigo = "1"
        orgao.nome = "1"
        orgao.save()

        self.assertIsNotNone(orgao.id)

    def test_save(self):
        orgao = Orgao()
        orgao.codigo = "1"
        orgao.nome = "1"
        orgao.save(force_insert=True)

        self.assertIsNotNone(orgao.id)

    def test_delete(self):
        self.orgao.delete()

        self.assertIsNone(self.orgao.id)


class PessoaTestCase(TestCase):
    def setUp(self) -> None:
        self.pessoa = mommy.make(Pessoa)

    def test_str(self):
        self.assertEqual(f'{self.pessoa.nome} - {self.pessoa.documento}', str(self.pessoa))


class EnderecoTestCase(TestCase):
    def setUp(self) -> None:
        self.endereco = mommy.make(Endereco)

    def test_str(self):
        self.assertEqual(f'{self.endereco.titulo} - {self.endereco.pessoa}', str(self.endereco))


class ContatoTestCase(TestCase):
    def setUp(self) -> None:
        self.contato = mommy.make(Contato)

    def test_str(self):
        self.assertEqual(f'{self.contato.titulo} - {self.contato.pessoa}', str(self.contato))


class ProfissionalTestCase(TestCase):
    def setUp(self) -> None:
        self.profissional = mommy.make(Profissional)

    def test_valor_do_pai(self):
        self.assertIsNotNone(self.profissional.documento)

    def test_str(self):
        self.assertEqual(f'{self.profissional.nome} - {self.profissional.orgao}', str(self.profissional))


class PlanoTestCase(TestCase):
    def setUp(self) -> None:
        self.plano = mommy.make(Plano)

    def test_str(self):
        self.assertEqual(f'{self.plano.nome} - {self.plano.codigo}', str(self.plano))


class ClienteTestCase(TestCase):
    def setUp(self) -> None:
        self.cliente = mommy.make(Cliente)

    def test_valor_do_pai(self):
        self.assertIsNotNone(self.cliente.documento)

    def test_str(self):
        self.assertEqual(f'{self.cliente.nome} - {self.cliente.plano} - {self.cliente.carteirinha}', str(self.cliente))


class CidTestCase(TestCase):
    def setUp(self) -> None:
        self.cid = mommy.make(Cid)

    def test_str(self):
        self.assertEqual(f'{self.cid.codigo} - {self.cid.descricao}', str(self.cid))


class ClienteCidTestCase(TestCase):
    def setUp(self) -> None:
        self.cliente_cid = mommy.make(ClienteCid)

    def test_str(self):
        self.assertEqual(f'{self.cliente_cid.cliente} - {self.cliente_cid.cid}', str(self.cliente_cid))


class UsuarioTestCase(TestCase):
    def setUp(self) -> None:
        self.usuario = mommy.make(Usuario)

    def test_valor_do_pai(self):
        self.assertIsNotNone(self.usuario.documento)

    def test_str(self):
        self.assertEqual(f'{self.usuario.nome} - {self.usuario.username}', str(self.usuario))

    def test_save(self):
        usuario = Usuario()
        usuario.nome = "9"
        usuario.nascimento = "1999-09-09"
        usuario.documento = "9"
        usuario.tipo_pessoa = "F"
        usuario.username = "9"
        usuario.email = "9@mail.com"
        usuario.senha = "9"
        usuario.save(force_insert=True)
        self.assertIsNotNone(usuario.id)


class PapelTestCase(TestCase):
    def setUp(self) -> None:
        self.papel = mommy.make(Papel)

    def test_str(self):
        self.assertEqual(f'{self.papel.codigo} - {self.papel.descricao}', str(self.papel))


class UsuarioPapelTestCase(TestCase):
    def setUp(self) -> None:
        self.usuario_papel = mommy.make(UsuarioPapel)

    def test_str(self):
        self.assertEqual(f'{self.usuario_papel.usuario} - {self.usuario_papel.papel}', str(self.usuario_papel))


class AtendimentoTestCase(TestCase):
    def setUp(self) -> None:
        self.atendimento = mommy.make(Atendimento)

    def test_str(self):
        self.assertEqual(f'{self.atendimento.cliente} - {self.atendimento.status} - {self.atendimento.agendamento}',
                         str(self.atendimento))


class PerguntaTestCase(TestCase):
    def setUp(self) -> None:
        self.pergunta = mommy.make(Pergunta)

    def test_str(self):
        self.assertEqual(f'{self.pergunta.tipo_pergunta} - {self.pergunta.descricao}', str(self.pergunta))


class PerguntaOpcaoTestCase(TestCase):
    def setUp(self) -> None:
        self.pergunta_opcao = mommy.make(PerguntaOpcao)

    def test_str(self):
        self.assertEqual(f'{self.pergunta_opcao.pergunta} - {self.pergunta_opcao.opcao}', str(self.pergunta_opcao))


class FormularioTestCase(TestCase):
    def setUp(self) -> None:
        self.formulario = mommy.make(Formulario)

    def test_str(self):
        self.assertEqual(f'{self.formulario.codigo} - {self.formulario.descricao}', str(self.formulario))


class FormularioPerguntaTestCase(TestCase):
    def setUp(self) -> None:
        self.formulario_pergunta = mommy.make(FormularioPergunta)

    def test_str(self):
        self.assertEqual(f'{self.formulario_pergunta.pergunta} - {self.formulario_pergunta.formulario}',
                         str(self.formulario_pergunta))


class FormularioPapelTestCase(TestCase):
    def setUp(self) -> None:
        self.formulario_papel = mommy.make(FormularioPapel)

    def test_str(self):
        self.assertEqual(f'{self.formulario_papel.papel} - {self.formulario_papel.formulario}',
                         str(self.formulario_papel))


class EvolucaoTestCase(TestCase):
    def setUp(self) -> None:
        self.evolucao = mommy.make(Evolucao)

    def test_str(self):
        self.assertEqual(f'{self.evolucao.formulario} - {self.evolucao.atendimento}', str(self.evolucao))


class EvolucaoRespostaTestCase(TestCase):
    def setUp(self) -> None:
        self.evolucao_resposta = mommy.make(EvolucaoResposta)

    def test_str(self):
        self.assertEqual(f'{self.evolucao_resposta.pergunta_texto} - {self.evolucao_resposta.resposta_texto}',
                         str(self.evolucao_resposta))


class SessaoTestCase(TestCase):
    def setUp(self) -> None:
        self.sessao = mommy.make(Sessao)

    def test_str(self):
        self.assertEqual(f'{self.sessao.usuario} - {self.sessao.inicio} -> {self.sessao.fim}', str(self.sessao))