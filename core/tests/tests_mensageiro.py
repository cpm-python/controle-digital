from django.test import TestCase
from core.mensageiro import Discord, gerar_cor_aleatoria


class MensagemiroTestCase(TestCase):

    def setUp(self):
        print(self)
        self.log = Discord()

    def test_enviar_simples(self):
        self.log.enviar_simples("simples")

    def test_gerar_cor_aleatoria(self):
        self.assertIsNotNone(gerar_cor_aleatoria())
