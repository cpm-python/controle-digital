from django.test import TestCase
from datetime import datetime, timedelta
from core.utils import *


class UtilTestCase(TestCase):
    def setUp(self) -> None:
        pass

    def test_criptografar_null(self):
        self.failUnlessRaises(Exception, criptografar, None)

    def test_data_agora(self):
        self.assertIsNotNone(data_agora())

    def test_somar_minutos(self):
        minutos = 30

        hora = datetime(2020, 5, 17, hour=0, minute=0, second=0, microsecond=0)
        hora_fim = datetime(2020, 5, 17, hour=0, minute=minutos, second=0, microsecond=0)

        self.assertEqual(hora_fim, data_somar_minutos(hora, minutos))
