from django.test import TestCase

from rest_framework.test import APIRequestFactory
from rest_framework.test import force_authenticate
from django.contrib.auth.models import User

from core.utils import data_agora, data_somar_minutos
from core.models import Usuario, Sessao
from core.views import SessaoViewSet

TOKEN_FAKE = 'ea8e857d-38aa-45b8-a22b-9d9ac5656f97'


class SessaoTestCase(TestCase):
    def setUp(self) -> None:

        self.username = 'config_tester'
        self.password = 'goldenstandard'
        self.user = User.objects.create_superuser(self.username, 'test@example.com', self.password)

        self.usuario = Usuario()
        self.usuario.nome = "Teste"
        self.usuario.nascimento = "1999-09-09"
        self.usuario.documento = "9"
        self.usuario.tipo_pessoa = "F"
        self.usuario.username = self.username
        self.usuario.email = "9@mail.com"
        self.usuario.senha = self.password
        self.usuario.save(force_insert=True)

        self.sessao = Sessao()
        self.sessao.usuario = self.usuario
        self.sessao.fim = data_somar_minutos(data_agora(), 30)
        self.sessao.save()

    def test_get_sessao(self):
        factory = APIRequestFactory()
        user = User.objects.get(username=self.username)
        detail_view = SessaoViewSet.as_view({'get': 'sessao'})

        api_request = factory.get('/sessao/')
        force_authenticate(api_request, user=user)
        response = detail_view(api_request, self.sessao.token)
        self.assertEqual(response.status_code, 201)

    def test_get_sessao_fail(self):
        factory = APIRequestFactory()
        user = User.objects.get(username=self.username)
        detail_view = SessaoViewSet.as_view({'get': 'sessao'})

        api_request = factory.get('/sessao/')
        force_authenticate(api_request, user=user)
        response = detail_view(api_request, TOKEN_FAKE)
        self.assertEqual(response.status_code, 401)

    def test_get_sessao_token_vazio(self):
        factory = APIRequestFactory()
        user = User.objects.get(username=self.username)
        detail_view = SessaoViewSet.as_view({'get': 'sessao'})

        api_request = factory.get('/sessao/')
        force_authenticate(api_request, user=user)
        response = detail_view(api_request, '')
        self.assertEqual(response.status_code, 401)

    def test_login(self):
        factory = APIRequestFactory()
        user = User.objects.get(username=self.username)
        detail_view = SessaoViewSet.as_view({'post': 'login'})

        body = {"usuario": self.username, "senha": self.password}
        api_request = factory.post('/login/', body)
        force_authenticate(api_request, user=user)
        response = detail_view(api_request)

        self.assertEqual(response.status_code, 201)

    def test_login_nova_sessao(self):
        """test_login_nova_sessao"""

        self.sessao.fim = data_agora()
        self.sessao.save()

        factory = APIRequestFactory()
        user = User.objects.get(username=self.username)
        detail_view = SessaoViewSet.as_view({'post': 'login'})

        body = {"usuario": self.username, "senha": self.password}
        api_request = factory.post('/login/', body)
        force_authenticate(api_request, user=user)
        response = detail_view(api_request)

        self.assertEqual(response.status_code, 201)

    def test_login_sem_usuario(self):
        factory = APIRequestFactory()
        user = User.objects.get(username=self.username)
        detail_view = SessaoViewSet.as_view({'post': 'login'})

        body = {"senha": self.password}
        api_request = factory.post('/login/', body)
        force_authenticate(api_request, user=user)
        response = detail_view(api_request)

        self.assertEqual(response.status_code, 400)

    def test_login_sem_senha(self):
        factory = APIRequestFactory()
        user = User.objects.get(username=self.username)
        detail_view = SessaoViewSet.as_view({'post': 'login'})

        body = {"usuario": self.username}
        api_request = factory.post('/login/', body)
        force_authenticate(api_request, user=user)
        response = detail_view(api_request)

        self.assertEqual(response.status_code, 400)

    def test_login_usuario_invalido(self):
        factory = APIRequestFactory()
        user = User.objects.get(username=self.username)
        detail_view = SessaoViewSet.as_view({'post': 'login'})

        body = {"usuario": "username", "senha": self.password}
        api_request = factory.post('/login/', body)
        force_authenticate(api_request, user=user)
        response = detail_view(api_request)

        self.assertEqual(response.status_code, 401)

    def test_logout(self):
        factory = APIRequestFactory()
        user = User.objects.get(username=self.username)
        detail_view = SessaoViewSet.as_view({'put': 'logout'})

        api_request = factory.put('/logout/')
        force_authenticate(api_request, user=user)
        response = detail_view(api_request, self.sessao.token)

        self.assertEqual(response.status_code, 201)

    def test_logout_sem_token(self):
        factory = APIRequestFactory()
        user = User.objects.get(username=self.username)
        detail_view = SessaoViewSet.as_view({'put': 'logout'})

        api_request = factory.put('/logout/')
        force_authenticate(api_request, user=user)
        response = detail_view(api_request, "")

        self.assertEqual(response.status_code, 401)

    def test_logout_token_invalido(self):
        factory = APIRequestFactory()
        user = User.objects.get(username=self.username)
        detail_view = SessaoViewSet.as_view({'put': 'logout'})

        api_request = factory.put('/logout/')
        force_authenticate(api_request, user=user)
        response = detail_view(api_request, TOKEN_FAKE)

        self.assertEqual(response.status_code, 401)
