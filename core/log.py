import logging
from core.mensageiro import Discord


class LOG:
    def __init__(self, titulo: str):
        self.logger = logging.getLogger(titulo)
        self.logger.setLevel(logging.DEBUG)
        self.discord = Discord()

        ch = logging.StreamHandler()
        ch.setLevel(logging.DEBUG)

        formatter = logging.Formatter('[%(asctime)s] **%(levelname)s** => %(name)s => %(message)s',
                                      datefmt='%d/%b/%Y %H:%M:%S')

        ch.setFormatter(formatter)

        self.logger.addHandler(ch)

    def debug(self, mensagem):
        self.discord.enviar(mensagem, cor=205, titulo='Debug')
        self.logger.debug(mensagem)

    def info(self, mensagem):
        self.discord.enviar(mensagem, cor=9287168, titulo='Info')
        self.logger.info(mensagem)

    def warning(self, mensagem):
        self.discord.enviar(mensagem, cor=16734976, titulo='Warning')
        self.logger.warning(mensagem)

    def error(self, mensagem):
        self.discord.enviar(mensagem, cor=16711680, titulo='Error')
        self.logger.error(mensagem)

    def critical(self, mensagem):
        self.discord.enviar(mensagem, 16711680, titulo='Critical')
        self.logger.critical(mensagem)
