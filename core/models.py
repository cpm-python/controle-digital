import uuid
from enum import Enum

from django.db import models
from core.log import LOG
from core.utils import criptografar

LOG = LOG('models')


class Base(models.Model):
    id = models.UUIDField(default=uuid.uuid4, primary_key=True, editable=False, null=False)
    criado = models.DateTimeField('Data de Criação', auto_now_add=True)
    modificado = models.DateTimeField('Data de Atualização', auto_now=True)
    ativo = models.BooleanField('Ativo?', default=True)
    excluido = models.BooleanField('Excluido', default=False)

    def generic_title(self):
        return f'{self.__class__.__name__} => {self.id} ==> {self}'

    def save(self, *args, **kwargs):
        tipo = "Atualizando"
        tipo_final = "Atualizado"

        if "force_insert" in kwargs and kwargs["force_insert"]:
            tipo = "Salvando"
            tipo_final = "Salvo"

        LOG.debug(f'{tipo}: {self.generic_title()}')
        super(Base, self).save(*args, **kwargs)
        LOG.debug(f'{tipo_final}: {self.generic_title()}')

    def delete(self, *args, **kwargs):
        title = self.generic_title()

        LOG.warning(f'Apagando: {title}')
        super(Base, self).delete(*args, **kwargs)
        LOG.warning(f'Apagado: {title}')

    class Meta:
        abstract = True


class TipoPessoa(Enum):
    F = 'Pessoa Física'
    J = 'Pessoa Jurídica'

    @classmethod
    def choices(cls):
        return tuple((i.name, i.value) for i in cls)


class Pessoa(Base):
    nascimento = models.DateField('Nascimento', null=False)
    documento = models.CharField('Documento', unique=True, null=False, max_length=15)
    tipo_pessoa = models.CharField('Tipo Pessoa', choices=TipoPessoa.choices(), null=False, max_length=1)
    nome = models.CharField('Nome', null=False, max_length=100, default='')

    class Meta:
        verbose_name = 'Pessoa'
        verbose_name_plural = 'Pessoas'
        ordering = ['nome']

    def __str__(self):
        return f'{self.nome} - {self.documento}'


class Orgao(Base):
    nome = models.CharField('Nome', null=False, max_length=100)
    codigo = models.CharField('Código', null=False, max_length=10)

    class Meta:
        verbose_name = 'Orgão'
        verbose_name_plural = 'Orgãos'
        ordering = ['codigo']

    def __str__(self):
        return self.codigo


class Estado(Enum):
    AC = 'Acre'
    AL = 'Alagoas'
    AP = 'Amapá'
    AM = 'Amazonas'
    BA = 'Bahia'
    CE = 'Ceará'
    DF = 'Distrito Federal'
    ES = 'Espírito Santo'
    GO = 'Goiás'
    MA = 'Maranhão'
    MT = 'Mato Grosso'
    MS = 'Mato Grosso do Sul'
    MG = 'Minas Gerais'
    PA = 'Pará'
    PB = 'Paraíba'
    PR = 'Paraná'
    PE = 'Pernambuco'
    PI = 'Piauí'
    RJ = 'Rio de Janeiro'
    RN = 'Rio Grande do Norte'
    RS = 'Rio Grande do Sul'
    RO = 'Rondônia'
    RR = 'Roraima'
    SC = 'Santa Catarina'
    SP = 'São Paulo'
    SE = 'Sergipe'
    TO = 'Tocantins'

    @classmethod
    def choices(cls):
        return tuple((i.name, i.value) for i in cls)


class Endereco(Base):
    pessoa = models.ForeignKey(Pessoa, related_name='enderecos', on_delete=models.CASCADE, null=False)
    titulo = models.CharField('Título', max_length=50, null=False, default='')
    logradouro = models.CharField('Logradouro', max_length=200, null=False)
    numero = models.IntegerField('Número', null=False)
    complemento = models.CharField('Complemento', max_length=15)
    cep = models.CharField('CEP', max_length=9, null=False)
    bairro = models.CharField('Bairro', max_length=100, null=False)
    cidade = models.CharField('Cidade', max_length=100, null=False)
    estado = models.CharField('Estado', max_length=2, choices=Estado.choices(), null=False)
    pais = models.CharField('País', max_length=100, null=False)

    class Meta:
        verbose_name = 'Enderço'
        verbose_name_plural = 'Enderços'
        ordering = ['pessoa', 'titulo', 'estado', 'cidade', 'logradouro', 'numero']

    def __str__(self):
        return f'{self.titulo} - {self.pessoa}'


class TipoContato(Enum):
    T = 'Telefone'
    E = 'E-mail'
    R = 'Rede Social'

    @classmethod
    def choices(cls):
        return tuple((i.name, i.value) for i in cls)


class Contato(Base):
    pessoa = models.ForeignKey(Pessoa, related_name='contatos', on_delete=models.CASCADE, null=False)
    tipo = models.CharField('Tipo de Contato', max_length=1, choices=TipoContato.choices(), null=False)
    titulo = models.CharField('Título', max_length=50, null=False)
    valor = models.CharField('Valor', max_length=50, null=False)

    class Meta:
        verbose_name = 'Contato'
        verbose_name_plural = 'Contatos'

    def __str__(self):
        return f'{self.titulo} - {self.pessoa}'


class Profissional(Pessoa):
    orgao = models.ForeignKey(Orgao, related_name='profissionais', on_delete=models.CASCADE, null=False)
    numero_orgao = models.CharField('Número órgão', max_length=15, null=False)
    estado_orgao = models.CharField('Estado', max_length=2, choices=Estado.choices(), null=False)

    class Meta:
        verbose_name = 'Profissional'
        verbose_name_plural = 'Profissionais'

    def __str__(self):
        return f'{self.nome} - {self.orgao}'


class Plano(Base):
    nome = models.CharField('Nome', max_length=100, null=False)
    codigo = models.CharField('Código', max_length=100, null=False)

    class Meta:
        verbose_name = 'Plano'
        verbose_name_plural = 'Planos'

    def __str__(self):
        return f'{self.nome} - {self.codigo}'


class Cliente(Pessoa):
    plano = models.ForeignKey(Plano, related_name='clientes', on_delete=models.CASCADE, null=False)
    carteirinha = models.CharField('Carteirinha', max_length=25, null=False)

    class Meta:
        verbose_name = 'Cliente'
        verbose_name_plural = 'Clientes'

    def __str__(self):
        return f'{self.nome} - {self.plano} - {self.carteirinha}'


class Cid(Base):
    codigo = models.CharField('Código', max_length=15, null=False)
    descricao = models.CharField('Descrição', max_length=200, null=False)

    class Meta:
        verbose_name = 'Cid'
        verbose_name_plural = 'Cids'

    def __str__(self):
        return f'{self.codigo} - {self.descricao}'


class ClienteCid(Base):
    cliente = models.ForeignKey(Cliente, related_name='clientes_cid', on_delete=models.CASCADE, null=False)
    cid = models.ForeignKey(Cid, related_name='cid_clientes', on_delete=models.CASCADE, null=False)
    data_diagnostico = models.DateField('Data de diagnóstico', null=False)

    class Meta:
        verbose_name = 'ClienteCid'
        verbose_name_plural = 'ClientesCid'

    def __str__(self):
        return f'{self.cliente} - {self.cid}'


class Usuario(Pessoa):
    username = models.CharField('Nome de usuário', max_length=25, null=False, unique=True)
    senha = models.CharField('Senha do usuário',
                             max_length=60,
                             null=False,
                             blank=False,
                             help_text="Favor inserir a senha")
    email = models.EmailField('Email do usuário', max_length=100, null=False, unique=True)

    def save(self, *args, **kwargs):
        tipo = "Atualizando"
        tipo_final = "Atualizado"

        if "force_insert" in kwargs and kwargs["force_insert"]:
            self.senha = criptografar(self.senha)
            tipo = "Salvando"
            tipo_final = "Salvo"

        LOG.debug(f'{tipo}: {self.generic_title()}')
        super(Base, self).save(*args, **kwargs)
        LOG.debug(f'{tipo_final}: {self.generic_title()}')

    class Meta:
        verbose_name = 'Usuário'
        verbose_name_plural = 'Usuários'
        ordering = ['nome']

    def __str__(self):
        return f'{self.nome} - {self.username}'


class Papel(Base):
    codigo = models.CharField('Código', max_length=15, null=False)
    descricao = models.CharField('Descrição', max_length=200, null=False)

    class Meta:
        verbose_name = 'Papel'
        verbose_name_plural = 'Papeis'

    def __str__(self):
        return f'{self.codigo} - {self.descricao}'


class UsuarioPapel(Base):
    usuario = models.ForeignKey(Usuario, related_name='usuarios_papel', on_delete=models.CASCADE, null=False)
    papel = models.ForeignKey(Papel, related_name='papel_usuarios', on_delete=models.CASCADE, null=False)
    data = models.DateField('Data de criação', null=False, auto_now=True)

    class Meta:
        verbose_name = 'UsuarioPapel'
        verbose_name_plural = 'UsuariosPapeis'

    def __str__(self):
        return f'{self.usuario} - {self.papel}'


class UsuarioProfissional(Base):
    usuario = models.ForeignKey(Usuario, related_name='usuarios_profissionais',
                                on_delete=models.CASCADE, null=False, unique=True)
    profissional = models.ForeignKey(Profissional, related_name='profissional_usuarios',
                                     on_delete=models.CASCADE, null=False, unique=True)

    class Meta:
        verbose_name = 'UsuarioProfissional'
        verbose_name_plural = 'Usuarios Profissionais'
        unique_together = ['usuario', 'profissional']

    def __str__(self):
        return f'{self.usuario} - {self.profissional}'


class AtedimentoStatus(Enum):
    AGENDADO = 'Agendado'
    ATENDENDO = 'Atendendo'
    ATENDIDO = 'Atendido'
    CANCELADO = 'Cancelado'

    @classmethod
    def choices(cls):
        return tuple((i.name, i.value) for i in cls)


class Atendimento(Base):
    cliente = models.ForeignKey(Cliente, related_name='atendimentos', on_delete=models.CASCADE, null=False)
    profissional = models.ForeignKey(Profissional, related_name='atendimentos', on_delete=models.CASCADE, null=False)
    status = models.CharField('Status do atendimento', max_length=100, choices=AtedimentoStatus.choices(),
                              default=AtedimentoStatus.AGENDADO, null=False)
    agendamento = models.DateTimeField('Data e hora do atendimento', null=False)

    class Meta:
        verbose_name = 'Atendimento'
        verbose_name_plural = 'Atendimentos'
        ordering = ['agendamento']

    def __str__(self):
        return f'{self.cliente} - {self.status} - {self.agendamento}'


class TipoPergunta(Enum):
    TEXT = 'Texto'
    TEXTAREA = 'Texto Grande'
    RADIO = 'Rádio'
    COMBO = 'Multiplas escolhas'
    BOOLEAN = "Sim/Não"
    BUTTON = 'button'
    CHECKBOX = 'checkbox'
    COLOR = 'color'
    DATE = 'date'
    EMAIL = 'email'
    NUMBER = 'number'
    RANGE = 'range'
    TIME = 'time'

    @classmethod
    def choices(cls):
        return tuple((i.name, i.value) for i in cls)


class Pergunta(Base):
    tipo_pergunta = models.CharField('Tipo de pergunta', null=False, max_length=10, choices=TipoPergunta.choices())
    descricao = models.CharField("Pergunta", null=False, max_length=140)

    class Meta:
        verbose_name = 'Pergunta'
        verbose_name_plural = 'Perguntas'

    def __str__(self):
        return f'{self.tipo_pergunta} - {self.descricao}'


class PerguntaOpcao(Base):
    pergunta = models.ForeignKey(Pergunta, related_name='opcoes', on_delete=models.CASCADE, null=False)
    opcao = models.CharField('Opção', max_length=200, null=False)

    class Meta:
        verbose_name = 'Opção da pergunta'
        verbose_name_plural = 'Opções da pergunta'

    def __str__(self):
        return f'{self.pergunta} - {self.opcao}'


class Formulario(Base):
    codigo = models.CharField('Código', max_length=15, null=False)
    descricao = models.CharField('Descrição', max_length=200, null=False)

    class Meta:
        verbose_name = 'Formulário'
        verbose_name_plural = 'Formulários'

    def __str__(self):
        return f'{self.codigo} - {self.descricao}'


class FormularioPergunta(Base):
    pergunta = models.ForeignKey(Pergunta, related_name='formularios_da_pergunta', on_delete=models.CASCADE, null=False)
    formulario = models.ForeignKey(Formulario, related_name='perguntas_do_formulario', on_delete=models.CASCADE,
                                   null=False)

    class Meta:
        verbose_name = 'Pergunta do Formulário'
        verbose_name_plural = 'Perguntas do Formulário'

    def __str__(self):
        return f'{self.pergunta} - {self.formulario}'


class FormularioPapel(Base):
    papel = models.ForeignKey(Papel, related_name='formularios_do_papel', on_delete=models.CASCADE, null=False)
    formulario = models.ForeignKey(Formulario, related_name='papeis_do_formulario', on_delete=models.CASCADE,
                                   null=False)

    class Meta:
        verbose_name = 'Papél do Formulário'
        verbose_name_plural = 'Papíes do Formulário'

    def __str__(self):
        return f'{self.papel} - {self.formulario}'


class Evolucao(Base):
    formulario = models.ForeignKey(Formulario, related_name='formularios', on_delete=models.CASCADE, null=False)
    atendimento = models.ForeignKey(Atendimento, related_name='evolucoes', on_delete=models.CASCADE, null=False)

    class Meta:
        verbose_name = 'Evolução'
        verbose_name_plural = 'Evoluções'
        ordering = ['-criado']

    def __str__(self):
        return f'{self.formulario} - {self.atendimento}'


class EvolucaoResposta(Base):
    evolucao = models.ForeignKey(Evolucao, related_name='respostas', on_delete=models.CASCADE, null=False)
    pergunta_texto = models.TextField('Pergunta', null=False)
    resposta_texto = models.TextField('Resposta', null=False)

    class Meta:
        verbose_name = 'Resposta da Evolução'
        verbose_name_plural = 'Respostas da Evolução'
        ordering = ['criado']

    def __str__(self):
        return f'{self.pergunta_texto} - {self.resposta_texto}'


class Sessao(models.Model):
    token = models.UUIDField(default=uuid.uuid4, primary_key=True, editable=False, null=False)
    usuario = models.ForeignKey(Usuario, related_name='usuarios', on_delete=models.CASCADE, null=False)
    inicio = models.DateTimeField('Data de inicio da sessao', auto_now_add=True)
    fim = models.DateTimeField('Data final da sessão')
    modificado = models.DateTimeField('Data de Atualização', auto_now=True)

    class Meta:
        verbose_name = 'Sessão'
        verbose_name_plural = 'Sessões'
        ordering = ['inicio']

    def __str__(self):
        return f'{self.usuario} - {self.inicio} -> {self.fim}'
