from rest_framework import serializers
from core.models import *


class PessoaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Pessoa
        fields = (
            'id',
            'nome',
            'nascimento',
            'documento',
            'tipo_pessoa',
        )


class OrgaoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Orgao
        fields = (
            'id',
            'nome',
            'codigo',
        )


class EnderecoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Endereco
        fields = (
            'id',
            'pessoa',
            'titulo',
            'logradouro',
            'numero',
            'complemento',
            'cep',
            'bairro',
            'cidade',
            'estado',
            'pais',
        )


class ContatoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Contato
        fields = (
            'id',
            'pessoa',
            'tipo',
            'titulo',
            'valor',
        )


class ProfissionalSerializer(serializers.ModelSerializer):
    enderecos = EnderecoSerializer(many=True, read_only=True)
    contatos = ContatoSerializer(many=True, read_only=True)

    class Meta:
        model = Profissional
        fields = (
            'id',
            'nome',
            'nascimento',
            'documento',
            'tipo_pessoa',
            'orgao',
            'numero_orgao',
            'estado_orgao',
            'enderecos',
            'contatos'
        )


class PlanoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Plano
        fields = (
            'id',
            'nome',
            'codigo',
        )


class CidSerializer(serializers.ModelSerializer):
    class Meta:
        model = Cid
        fields = (
            'id',
            'codigo',
            'descricao',
        )


class ClienteCidSerializerList(serializers.ModelSerializer):
    cid = CidSerializer(many=False, read_only=True)

    class Meta:
        model = ClienteCid
        fields = (
            'id',
            'cliente',
            'cid',
            'data_diagnostico',
        )


class AtendimentoSerializer(serializers.ModelSerializer):

    class Meta:
        model = Atendimento
        fields = (
            'id',
            'cliente',
            'profissional',
            'status',
            'agendamento',
        )


class ClienteSerializer(serializers.ModelSerializer):
    enderecos = EnderecoSerializer(many=True, read_only=True)
    contatos = ContatoSerializer(many=True, read_only=True)
    clientes_cid = ClienteCidSerializerList(many=True, read_only=True)
    atendimentos = AtendimentoSerializer(many=True, read_only=True)

    class Meta:
        model = Cliente
        fields = (
            'id',
            'nome',
            'nascimento',
            'documento',
            'tipo_pessoa',
            'plano',
            'carteirinha',
            'enderecos',
            'contatos',
            'clientes_cid',
            'atendimentos'
        )


class ClienteCidSerializer(serializers.ModelSerializer):
    class Meta:
        model = ClienteCid
        fields = (
            'id',
            'cliente',
            'cid',
            'data_diagnostico',
        )


class PapelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Papel
        fields = (
            'id',
            'codigo',
            'descricao',
        )


class UsuarioPapelSerializer(serializers.ModelSerializer):

    class Meta:
        model = UsuarioPapel
        fields = (
            'id',
            'usuario',
            'papel',
            'data',
        )


class UsuarioPapelSerializerList(serializers.ModelSerializer):

    papel = PapelSerializer(many=False, read_only=False, partial=True)

    class Meta:
        model = UsuarioPapel
        fields = (
            'id',
            'usuario',
            'papel',
            'data',
        )


class UsuarioProfissionalSerializer(serializers.ModelSerializer):

    class Meta:
        model = UsuarioProfissional
        fields = (
            'id',
            'usuario',
            'profissional',
        )


class UsuarioProfissionalSerializerList(serializers.ModelSerializer):

    profissional = ProfissionalSerializer(many=False, read_only=False, partial=True)

    class Meta:
        model = UsuarioProfissional
        fields = (
            'id',
            'usuario',
            'profissional',
        )


class UsuarioSerializer(serializers.ModelSerializer):
    enderecos = EnderecoSerializer(many=True, read_only=True)
    contatos = ContatoSerializer(many=True, read_only=True)
    usuarios_papel = UsuarioPapelSerializerList(many=True, read_only=True)
    usuarios_profissionais = UsuarioProfissionalSerializerList(many=True, read_only=True)

    class Meta:
        model = Usuario
        fields = (
            'id',
            'nome',
            'nascimento',
            'documento',
            'tipo_pessoa',
            'username',
            'email',
            'enderecos',
            'contatos',
            'senha',
            'usuarios_papel',
            'usuarios_profissionais',
        )


class PerguntaOpcaoSerializer(serializers.ModelSerializer):
    class Meta:
        model = PerguntaOpcao
        fields = (
            'id',
            'pergunta',
            'opcao',
        )


class PerguntaSerializer(serializers.ModelSerializer):
    opcoes = PerguntaOpcaoSerializer(many=True, read_only=True)

    class Meta:
        model = Pergunta
        fields = (
            'id',
            'tipo_pergunta',
            'descricao',
            'opcoes',
        )


class FormularioPerguntaSerializerList(serializers.ModelSerializer):

    pergunta = PerguntaSerializer(many=False, read_only=True)

    class Meta:
        model = FormularioPergunta
        fields = (
            'id',
            'pergunta',
            'formulario',
        )


class FormularioPapelSerializerList(serializers.ModelSerializer):

    papel = PapelSerializer(many=False, read_only=True)

    class Meta:
        model = FormularioPergunta
        fields = (
            'id',
            'papel',
            'formulario',
        )


class FormularioSerializer(serializers.ModelSerializer):
    perguntas_do_formulario = FormularioPerguntaSerializerList(many=True, read_only=True)
    papeis_do_formulario = FormularioPapelSerializerList(many=True, read_only=True)

    class Meta:
        model = Formulario
        fields = (
            'id',
            'codigo',
            'descricao',
            'perguntas_do_formulario',
            "papeis_do_formulario"
        )


class EvolucaoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Evolucao
        fields = (
            'id',
            'formulario',
            'atendimento',
        )


class EvolucaoRespostaSerializer(serializers.ModelSerializer):
    class Meta:
        model = EvolucaoResposta
        fields = (
            'id',
            'evolucao',
            'pergunta_texto',
            'resposta_texto',
        )


class EvolucaoSerializerList(serializers.ModelSerializer):
    formulario = FormularioSerializer(many=False, read_only=False)
    respostas = EvolucaoRespostaSerializer(many=True, read_only=False)

    class Meta:
        model = Evolucao
        fields = (
            'id',
            'formulario',
            'atendimento',
            'criado',
            'respostas',
        )


class AtendimentoSerializerGet(serializers.ModelSerializer):
    cliente = ClienteSerializer(many=False, read_only=False)
    profissional = ProfissionalSerializer(many=False, read_only=False)
    evolucoes = EvolucaoSerializerList(many=True, read_only=True)

    class Meta:
        model = Atendimento
        fields = (
            'id',
            'cliente',
            'profissional',
            'status',
            'agendamento',
            'evolucoes',
        )


class FormularioPerguntaSerializer(serializers.ModelSerializer):
    class Meta:
        model = FormularioPergunta
        fields = (
            'id',
            'pergunta',
            'formulario',
        )


class FormularioPapelSerializer(serializers.ModelSerializer):
    class Meta:
        model = FormularioPapel
        fields = (
            'id',
            'papel',
            'formulario',
        )


class SessaoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Sessao
        fields = (
            'usuario',
            'token',
            'fim'
        )
