from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from rest_framework import status

from core.models import Sessao
from core.log import LOG
from core.utils import data_agora, data_somar_minutos

TOKEN_INVALIDO = "Token Inválido"

path_livres = ['/auth/login/',
               '/auth/logout/',
               '/api/v2/sessao/login/',
               '/api/v2/sessao/logout/',
               '/admin/']


def criar_resposta_erro(tipo_status, memsagem):
    response = Response(status=tipo_status)
    response.accepted_renderer = JSONRenderer()
    response.accepted_media_type = "application/json"
    response.renderer_context = {"message": memsagem}
    response.content = memsagem
    return response


def validar_urls_livres(request):
    return request.path in path_livres or request.path.startswith('/admin/') or request.path.startswith('/swagger/')


class SessionMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response
        self.log = LOG('middleware')

    def __call__(self, request):
        return self.get_response(request)

    def process_view(self, request, view_func, view_args, view_kwargs):
        if validar_urls_livres(request):
            self.log.info(f'{request.user} em URL livre: {request.path}')
            return

        try:
            token = view_kwargs['token']
        except KeyError:
            return criar_resposta_erro(status.HTTP_401_UNAUTHORIZED, TOKEN_INVALIDO)

        if not token:
            return criar_resposta_erro(status.HTTP_401_UNAUTHORIZED, TOKEN_INVALIDO)

        agora = data_agora()

        sessao_lista = Sessao.objects.filter(token=token,
                                             inicio__lte=agora,
                                             fim__gte=agora)

        if not sessao_lista:
            return criar_resposta_erro(status.HTTP_401_UNAUTHORIZED, TOKEN_INVALIDO)

        sessao_lista[0].fim = data_somar_minutos(agora, 30)
        sessao_lista[0].save()

        self.log.info(f'{sessao_lista[0].usuario.username} acessando: {request.path} via {request.method}')

    def process_template_response(self, request, response):
        return response

    def process_exception(self, request, exception):
        texto_exception = f'{request.method} => **{request.path}**:\n{str(exception)}'

        self.log.error(texto_exception)

        return criar_resposta_erro(status.HTTP_500_INTERNAL_SERVER_ERROR, texto_exception)
