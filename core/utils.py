from core.log import LOG
import hashlib
from datetime import datetime, timedelta

LOG = LOG('utils')


def criptografar(senha):
    if not senha:
        raise Exception("Senha inválida")

    LOG.debug(f'Criptografando senha')
    return hashlib.md5(senha.encode('utf-8')).hexdigest()


def data_agora():
    LOG.info("Pegando a hora atual")
    return datetime.now()


def data_somar_minutos(data_hora: datetime, minutos: int):
    LOG.info(f"Adicionando {minutos} minutos a {data_hora}")
    return data_hora + timedelta(minutes=minutos)
