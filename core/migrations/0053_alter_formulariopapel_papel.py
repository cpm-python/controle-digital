# Generated by Django 3.2 on 2021-09-25 20:54

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0052_auto_20210925_1739'),
    ]

    operations = [
        migrations.AlterField(
            model_name='formulariopapel',
            name='papel',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='formularios_do_papel', to='core.papel'),
        ),
    ]
