# Generated by Django 3.2 on 2021-10-05 14:47

from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0053_alter_formulariopapel_papel'),
    ]

    operations = [
        migrations.CreateModel(
            name='UsuarioProfissional',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('criado', models.DateTimeField(auto_now_add=True, verbose_name='Data de Criação')),
                ('modificado', models.DateTimeField(auto_now=True, verbose_name='Data de Atualização')),
                ('ativo', models.BooleanField(default=True, verbose_name='Ativo?')),
                ('excluido', models.BooleanField(default=False, verbose_name='Excluido')),
                ('profissional', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='profissional_usuarios', to='core.profissional')),
                ('usuario', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='usuarios_profissionais', to='core.usuario')),
            ],
            options={
                'verbose_name': 'UsuarioProfissional',
                'verbose_name_plural': 'Usuarios Profissionais',
            },
        ),
    ]
