# Generated by Django 3.2 on 2021-06-13 09:57

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0047_alter_usuario_senha'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sessao',
            name='fim',
            field=models.DateTimeField(verbose_name='Data final da sessão'),
        ),
        migrations.AlterField(
            model_name='sessao',
            name='usuario',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='usuarios', to='core.usuario'),
        ),
    ]
