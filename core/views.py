from rest_framework import generics
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework import status

from core.utils import data_agora, data_somar_minutos
from core.serializers import *

"""
API V1
"""


class OrgaosAPIView(generics.ListCreateAPIView):
    """
    Get e Post de **Órgãos**, sem ID
    """
    queryset = Orgao.objects.all()
    serializer_class = OrgaoSerializer


class OrgaoAPIView(generics.RetrieveUpdateDestroyAPIView):
    """
    Get, Put, e Delete de **Órgãos** com ID
    """
    queryset = Orgao.objects.all()
    serializer_class = OrgaoSerializer


class EnderecosAPIView(generics.ListCreateAPIView):
    """
    Get e Post de **Endereco**, sem ID
    """
    queryset = Endereco.objects.all()
    serializer_class = EnderecoSerializer


class EnderecoAPIView(generics.RetrieveUpdateDestroyAPIView):
    """
    Get, Put, e Delete de **Endereco** com ID
    """
    queryset = Orgao.objects.all()
    serializer_class = EnderecoSerializer


"""
API V2
"""


class BaseViewSet(viewsets.ModelViewSet):
    lookup_field = 'id'

    class Meta:
        abstract = True


class OrgaoViewSet(BaseViewSet):
    """
    **Orgão** Crud
    """
    queryset = Orgao.objects.all()
    serializer_class = OrgaoSerializer


class EnderecoViewSet(BaseViewSet):
    """
    **Endereço** Crud
    """
    queryset = Endereco.objects.all()
    serializer_class = EnderecoSerializer


class ContatoViewSet(BaseViewSet):
    """
    **Contato** Crud
    """
    queryset = Contato.objects.all()
    serializer_class = ContatoSerializer


class ProfissionalViewSet(BaseViewSet):
    """
    **Profissional** Crud
    """
    queryset = Profissional.objects.all()
    serializer_class = ProfissionalSerializer


class PlanoViewSet(BaseViewSet):
    """
    **Plano** Crud
    """
    queryset = Plano.objects.all()
    serializer_class = PlanoSerializer


class ClienteViewSet(BaseViewSet):
    """
    **Cliente** Crud
    """
    queryset = Cliente.objects.all()
    serializer_class = ClienteSerializer


class CidViewSet(BaseViewSet):
    """
    **Cid** Crud
    """
    queryset = Cid.objects.all()
    serializer_class = CidSerializer


class ClienteCidViewSet(BaseViewSet):
    """
    **Cids do Cliente** Crud
    """
    queryset = ClienteCid.objects.all()
    serializer_class = ClienteCidSerializer


class UsuarioViewSet(BaseViewSet):
    """
    **Usuário** Crud
    """
    queryset = Usuario.objects.all()
    serializer_class = UsuarioSerializer


class PapelViewSet(BaseViewSet):
    """
    **Papel** Crud
    """
    queryset = Papel.objects.all()
    serializer_class = PapelSerializer


class UsuarioPapelViewSet(BaseViewSet):
    """
    **Papéis do usuário** Crud
    """
    queryset = UsuarioPapel.objects.all()
    serializer_class = UsuarioPapelSerializer


class UsuarioProfissionalViewSet(BaseViewSet):
    """
    **Profissional do usuário** Crud
    """
    queryset = UsuarioProfissional.objects.all()
    serializer_class = UsuarioProfissionalSerializer

    def get_serializer_class(self):
        if self.action == 'list' or self.action == 'retrieve':
            return UsuarioProfissionalSerializerList

        return UsuarioProfissionalSerializer


class AtendimentoViewSet(BaseViewSet):
    """
    **Atendimento** Crud
    """
    queryset = Atendimento.objects.all()
    serializer_class = AtendimentoSerializer

    def get_serializer_class(self):
        if self.action == 'list' or self.action == 'retrieve':
            return AtendimentoSerializerGet

        return AtendimentoSerializer

    @action(detail=False, methods=['get'], url_path='profissional/(?P<profissional>[^/.]+)')
    def atendimento_por_profissional(self, request, profissional, *args, **kwargs):
        if not profissional:
            return Response(status=status.HTTP_401_UNAUTHORIZED)

        sessao_lista = Atendimento.objects.filter(profissional=profissional)

        if not sessao_lista:
            return Response(status=status.HTTP_401_UNAUTHORIZED)

        return Response(status=status.HTTP_201_CREATED, data=AtendimentoSerializerGet(sessao_lista, many=True).data)


class PerguntaViewSet(BaseViewSet):
    """
    **Pergunta** Crud
    """
    queryset = Pergunta.objects.all()
    serializer_class = PerguntaSerializer


class PerguntaOpcaoViewSet(BaseViewSet):
    """
    **Opções da pergunta** Crud
    """
    queryset = PerguntaOpcao.objects.all()
    serializer_class = PerguntaOpcaoSerializer


class FormularioPerguntaViewSet(BaseViewSet):
    """
    **Perguntas do formulário** Crud
    """
    queryset = FormularioPergunta.objects.all()
    serializer_class = FormularioPerguntaSerializer


class FormularioPapelViewSet(BaseViewSet):
    """
    **Papéis do formulário** Crud
    """
    queryset = FormularioPapel.objects.all()
    serializer_class = FormularioPapelSerializer


class FormularioViewSet(BaseViewSet):
    """
    **Formulário** Crud
    """
    queryset = Formulario.objects.all()
    serializer_class = FormularioSerializer


class EvolucaoViewSet(BaseViewSet):
    """
    **Evoluções** Crud
    """
    queryset = Evolucao.objects.all()
    serializer_class = EvolucaoSerializer


class EvolucaoRespostaViewSet(BaseViewSet):
    """
    **Respostas da Evolução** Crud
    """
    queryset = EvolucaoResposta.objects.all()
    serializer_class = EvolucaoRespostaSerializer


class SessaoViewSet(viewsets.GenericViewSet):
    """
    Gerenciamento de sessão
    """
    queryset = Sessao.objects.all()
    serializer_class = SessaoSerializer

    @action(detail=False, methods=['get'], url_path='(?P<token>[^/.]+)')
    def sessao(self, request, token, *args, **kwargs):
        if not token:
            return Response(status=status.HTTP_401_UNAUTHORIZED)

        agora = data_agora()

        sessao_lista = Sessao.objects.filter(token=token,
                                             inicio__lte=agora,
                                             fim__gte=agora)

        if not sessao_lista:
            return Response(status=status.HTTP_401_UNAUTHORIZED)

        return Response(status=status.HTTP_201_CREATED, data=SessaoSerializer(sessao_lista[0], many=False).data)

    @action(detail=False, methods=['post'])
    def login(self, request, *args, **kwargs):
        body = request.data

        if body is None or 'senha' not in body or 'usuario' not in body:
            return Response(status=status.HTTP_400_BAD_REQUEST,
                            data='Favor enviar usuário e senha no body')

        senha = criptografar(body['senha'])

        usuario = Usuario.objects.filter(username=body['usuario'],
                                         senha=senha)
        if not usuario:
            return Response(status=status.HTTP_401_UNAUTHORIZED)

        agora = data_agora()

        sessao_lista = Sessao.objects.filter(usuario=usuario[0],
                                             inicio__lte=agora,
                                             fim__gte=agora)

        if not sessao_lista:
            sessao = Sessao()
            sessao.usuario = usuario[0]
            sessao.fim = data_somar_minutos(agora, 30)
            sessao.save()

            sessao_lista = [sessao]

        LOG.info(f'\'{sessao_lista[0].usuario.username}\' iniciando sessão até {sessao_lista[0].fim}')

        return Response(status=status.HTTP_201_CREATED, data=SessaoSerializer(sessao_lista[0], many=False).data)

    @action(detail=False, methods=['put'], url_path='logout/(?P<token>[^/.]+)')
    def logout(self, request, token, *args, **kwargs):

        if not token:
            return Response(status=status.HTTP_401_UNAUTHORIZED)

        agora = data_agora()

        sessao_lista = Sessao.objects.filter(token=token,
                                             inicio__lte=agora,
                                             fim__gte=agora)

        if not sessao_lista:
            return Response(status=status.HTTP_401_UNAUTHORIZED)

        sessao_lista[0].fim = agora
        sessao_lista[0].save()

        LOG.info(f'\'{sessao_lista[0].usuario.username}\' finalizando sessão')

        return Response(status=status.HTTP_201_CREATED, data=SessaoSerializer(sessao_lista[0], many=False).data)
