from django.contrib import admin

from .models import *


@admin.register(Orgao)
class OrgaoAdmin(admin.ModelAdmin):
    list_display = ('nome', 'codigo',)


@admin.register(Endereco)
class EnderecoAdmin(admin.ModelAdmin):
    list_display = ('pessoa', 'titulo',)


@admin.register(Contato)
class ContatoAdmin(admin.ModelAdmin):
    list_display = ('pessoa', 'titulo',)


@admin.register(Profissional)
class ProfissionalAdmin(admin.ModelAdmin):
    list_display = ('nome', 'orgao',)


@admin.register(Plano)
class PlanoAdmin(admin.ModelAdmin):
    list_display = ('nome', 'codigo',)


@admin.register(Cliente)
class ClienteAdmin(admin.ModelAdmin):
    list_display = ('nome', 'nascimento', 'carteirinha',)


@admin.register(Cid)
class CidAdmin(admin.ModelAdmin):
    list_display = ('codigo', 'descricao',)


@admin.register(Usuario)
class UsuarioAdmin(admin.ModelAdmin):
    list_display = ('nome', 'username', 'email',)


@admin.register(Papel)
class PapelAdmin(admin.ModelAdmin):
    list_display = ('codigo', 'descricao',)


@admin.register(Pergunta)
class PerguntaAdmin(admin.ModelAdmin):
    list_display = ('descricao', 'tipo_pergunta')


@admin.register(PerguntaOpcao)
class PerguntaOpcaoAdmin(admin.ModelAdmin):
    list_display = ('pergunta', 'opcao',)


@admin.register(Formulario)
class FormularioAdmin(admin.ModelAdmin):
    list_display = ('codigo', 'descricao',)
