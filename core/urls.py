from django.urls import path
from rest_framework.routers import SimpleRouter

from .views import *


router = SimpleRouter()
router.register(r'atendimento/(?P<token>[^/.]+)', AtendimentoViewSet)
router.register(r'cid/(?P<token>[^/.]+)', CidViewSet)
router.register(r'cliente/(?P<token>[^/.]+)', ClienteViewSet)
router.register(r'cliente_cid/(?P<token>[^/.]+)', ClienteCidViewSet)
router.register(r'contato/(?P<token>[^/.]+)', ContatoViewSet)
router.register(r'endereco/(?P<token>[^/.]+)', EnderecoViewSet)
router.register(r'evolucao/(?P<token>[^/.]+)', EvolucaoViewSet)
router.register(r'evolucao_resposta/(?P<token>[^/.]+)', EvolucaoRespostaViewSet)
router.register(r'formulario/(?P<token>[^/.]+)', FormularioViewSet)
router.register(r'formulario_papel/(?P<token>[^/.]+)', FormularioPapelViewSet)
router.register(r'formulario_pergunta/(?P<token>[^/.]+)', FormularioPerguntaViewSet)
router.register(r'orgao/(?P<token>[^/.]+)', OrgaoViewSet)
router.register(r'papel/(?P<token>[^/.]+)', PapelViewSet)
router.register(r'pergunta/(?P<token>[^/.]+)', PerguntaViewSet)
router.register(r'pergunta_opcao/(?P<token>[^/.]+)', PerguntaOpcaoViewSet)
router.register(r'plano/(?P<token>[^/.]+)', PlanoViewSet)
router.register(r'profissional/(?P<token>[^/.]+)', ProfissionalViewSet)
router.register(r'usuario/(?P<token>[^/.]+)', UsuarioViewSet)
router.register(r'usuario_papel/(?P<token>[^/.]+)', UsuarioPapelViewSet)
router.register(r'usuario_profissional/(?P<token>[^/.]+)', UsuarioProfissionalViewSet)

router.register(r'sessao', SessaoViewSet)

urlpatterns = [
    path('orgao/<str:token>/', OrgaosAPIView.as_view(), name='orgaos'),
    path('orgao/<str:token>/<str:pk>/', OrgaoAPIView.as_view(), name='orgao'),
    path('endereco/<str:token>/', EnderecosAPIView.as_view(), name='enderecos'),
    path('endereco/<str:token>/<str:pk>/', EnderecoAPIView.as_view(), name='endereco'),
]
