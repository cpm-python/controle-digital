# Controle Digital

[Python](https://www.python.org/) é uma linguagem de programação de alto nível, interpretada de script, imperativa, orientada a objetos, funcional, de tipagem dinâmica e forte. Foi lançada por Guido van Rossum em 1991.

Para o sistema o Python foi escolhido por ser uma linguagem leve e com possibilidades de uso de Machine Learning para uso no futuro. 


## Componentes

- [Django](https://www.djangoproject.com/)
- [Django Rest Framework](https://www.django-rest-framework.org/)
- [django-cors-headers](https://github.com/adamchainz/django-cors-headers)
- [drf-yasg](https://github.com/axnsan12/drf-yasg)
- [psycopg2](https://pypi.org/project/psycopg2/)
- [Rauth](https://rauth.readthedocs.io/en/latest/)

## Instalando

- [Instalando GIT](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
- [Baixar o projeto](https://gitlab.com/cpm-python/controle-digital/)
- [Instalando Python](https://gitlab.com/cpm-python/controle-digital/-/wikis/Instalando-Python)
- [Instalando Postgresql](https://gitlab.com/cpm-python/controle-digital/-/wikis/Instalando-Postgresql)
- [Instalando Django](https://gitlab.com/cpm-python/controle-digital/-/wikis/Instalando-Django)
- [Primeira vez no sistema](https://gitlab.com/cpm-python/controle-digital/-/wikis/Primeira-vez-no-Sistema)

## Rodando

Para executar o projeto basta rodar o seguinte comando:


```ssh
python3 manage.py runserver
```

Basta abrir o seguinte link: http://localhost:8000/

Para informações sobre as APIs abra o seguinte link: http://localhost:8000/swagger/

## Demais projetos

- [Link para o projeto FE](https://gitlab.com/cpm-react/igti).