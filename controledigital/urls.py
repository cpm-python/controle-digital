from django.contrib import admin
from django.urls import path, include
from core.urls import router

from django.conf.urls import url
from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi

titulo = 'Controle Digital'
descricao = 'Criar um MVP (Produto mínimo viável) de um sistema de prontuário eletrônico realizando ' \
            'agendamento, evolução do paciente e exibindo um histórico de atendimentos até o final do ' \
            'curso. Utilizando as tecnologias de React para a parte de frontend e Python para backend. ' \
            'Fazendo a comunicação entre elas usando o Rest. Usando um banco de dados grátis, de ' \
            'preferência o PostgreSQL. Para o versionamento será usado o Git.'

schema_view = get_schema_view(
    openapi.Info(
        title=titulo,
        default_version='v1',
        description=descricao,
        terms_of_service="https://www.google.com/policies/terms/",
        contact=openapi.Contact(email="cassiano91@gmail.com"),
    ),
    public=True,
    permission_classes=[permissions.AllowAny],
)

urlpatterns = [
    path('api/v1/', include('core.urls')),
    path('api/v2/', include(router.urls)),
    path('admin/', admin.site.urls),
    path('auth/', include('rest_framework.urls')),

    url(r'^swagger(?P<format>\.json|\.yaml)$', schema_view.without_ui(cache_timeout=0), name='schema-json'),
    url(r'^swagger/$', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    url(r'^redoc/$', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
]

admin.site.site_header = titulo
admin.site.site_title = titulo
admin.site.index_title = 'Gerenciamento do projeto'
